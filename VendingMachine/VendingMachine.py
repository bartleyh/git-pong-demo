class VendingMachine(object):

    def __init__(self):
        self.validcoins = {"nickel": 0.1, "dime": 0.2, "quarter": 0.25}
        self.balance = 0
        self.products = {"cola": 1.0, "crisps": 0.5, "chocolate": 0.65}


    def UpdateBalance(self, new_message):
        print('Your current balance is: ', round(new_message, 2))
        return new_message


    def AcceptCoins(self):
        product = self.ChooseProduct()
        print('Your choice of', product, 'costs $', self.products[product])
        
        while self.balance < self.products[product]:
            print('Please insert', round(self.products[product] - self.balance, 2))
            coin = input("Please insert a coin (type nickel, dime or quarter): ")
            
            if coin in self.validcoins.keys():
                self.balance += self.validcoins[coin]
                self.UpdateBalance(self.balance)
            elif coin == 'penny':
                self.UpdateBalance('We do not accept pennies, please try again')
                self.AcceptCoins()
            else:
                self.UpdateBalance('Invalid input, please try again')
                self.AcceptCoins()
        self.GiveChange(self.balance, product)
        print('Thank you!')
        self.balance = 0
        self.AcceptCoins()


    def GiveChange(self, Balance, product):
        if Balance > self.products[product]:
            ChangeDue = Balance - self.products[product]
            print("Your change is", round(ChangeDue, 2))


    def DisplayProducts(self):
        for key, value in self.products.items():
            print(key, value)


    def ChooseProduct(self):
        self.DisplayProducts()
        product = input("Please Choose a product: ")
        if product in self.products.keys():
            return product


if __name__ == '__main__':
    V = VendingMachine()
    V.AcceptCoins()
